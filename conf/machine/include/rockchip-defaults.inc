# meta-rockchip default settings

# kernel
PREFERRED_PROVIDER_virtual/kernel ?= "linux-yocto"
KCONFIG_MODE ?= "alldefconfig"
LINUX_VERSION_EXTENSION ?= "-rockchip"

MACHINE_EXTRA_RRECOMMENDS_append = " \
	${RK_WIFIBT_FIRMWARES} \
	kernel-modules \
"

# Custom kernel might not support newest python
PACKAGECONFIG_remove_pn-perf += " scripting"

PREFERRED_PROVIDER_virtual/egl ?= "rockchip-libmali"
PREFERRED_PROVIDER_virtual/libgles1 ?= "rockchip-libmali"
PREFERRED_PROVIDER_virtual/libgles2 ?= "rockchip-libmali"
PREFERRED_PROVIDER_virtual/libgles3 ?= "rockchip-libmali"
PREFERRED_PROVIDER_virtual/libopencl ?= "rockchip-libmali"
PREFERRED_PROVIDER_virtual/libgbm ?= "rockchip-libmali"
PACKAGECONFIG_pn-wayland = "${@ 'no-egl' if 'wayland' in d.getVar('RK_MALI_LIB') else '' }"

# HACK: Use libmali's KHR headers
PACKAGECONFIG_append_pn-mesa-gl += " no-khr-headers"
DEPENDS_append_pn-mesa-gl += " rockchip-libmali"

PREFERRED_PROVIDER_virtual/libgl ?= "mesa-gl"
PREFERRED_PROVIDER_virtual/mesa ?= "mesa-gl"

# xserver
PREFERRED_PROVIDER_virtual/xserver = "xserver-xorg"
XSERVER = " \
	xserver-xorg \
	xserver-xorg-utils \
	xserver-xorg-xvfb \
	xserver-xorg-extension-glx \
	xserver-xorg-module-libwfb \
	xserver-xorg-module-exa \
	xf86-video-modesetting \
	xf86-input-evdev \
	xf86-input-mouse \
	xf86-input-keyboard \
	"

# misc
IMAGE_FSTYPES += "ext4"

# boot device (sd-card/emmc)
RK_BOOT_DEVICE ??= "mmcblk0"
WICVARS_append = " RK_BOOT_DEVICE"

